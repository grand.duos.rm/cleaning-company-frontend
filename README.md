# Cleaning Company - Frontend

Este é o frontend para o aplicativo Cleaning Company, uma plataforma para gerenciar clientes, rotas de atendimento e otimizar a visita aos clientes da empresa.

## Tecnologias Utilizadas

- **React:** Biblioteca JavaScript para construção de interfaces de usuário.
- **Material-UI:** Biblioteca de componentes React para criar interfaces de usuário com design atraente e responsivo.
- **Axios:** Cliente HTTP baseado em Promise para fazer requisições AJAX.
- **TypeScript:** Superset de JavaScript que adiciona tipagem estática e outras funcionalidades.

## Instalação

1. Clone este repositório em sua máquina local:

   ```
   git clone https://gitlab.com/grand.duos.rm/cleaning-company-frontend
   ```

2. Acesse o diretório do projeto:

   ```
   cd cleaning-company-frotend
   ```

3. Instale as dependências:

   ```
   npm install
   ```

4. Configure as variáveis de ambiente criando um arquivo `.env` na raiz do projeto e adicionando as seguintes variáveis:

   ```
   BACKEND_API_BASE_URL='http://localhost:3000'
   ```

4. Execute o servidor:

   ```
   npm run start
   ```

## Abordagens e Escolhas

### React

React foi escolhido como biblioteca para construção de interfaces de usuário devido à sua popularidade, eficiência e facilidade de uso.

### Material-UI

Material-UI foi escolhido como biblioteca de componentes React devido à sua grande variedade de componentes pré-construídos, suporte a temas personalizáveis e integração com React.

### Axios

Axios foi escolhido como cliente HTTP devido à sua facilidade de uso, suporte a Promise e capacidade de realizar requisições AJAX de forma simples e eficiente.

### TypeScript

TypeScript foi escolhido como linguagem de programação devido aos benefícios de adicionar tipagem estática ao JavaScript, o que torna o código mais legível, mantém a consistência e reduz erros durante o desenvolvimento.

## Componentes Principais

- **Home:** Página inicial que exibe a lista de clientes e oferece funcionalidades como pesquisa, adição, edição e exclusão de clientes.
- **ClientList:** Componente para exibir a lista de clientes.
- **AddClientModal:** Modal para adicionar novos clientes.
- **EditClientModal:** Modal para editar informações de clientes existentes.
- **CalculateRouteModal:** Modal para calcular e exibir a rota ótima de visita aos clientes.


## Requisitos do Ambiente

- Node.js versão 18 ou superior
- O projeto será executado na porta 4000
- O projeto backend deve estar em execução
