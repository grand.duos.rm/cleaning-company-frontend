import React from 'react';

interface Client { 
  nome: string;
  email: string;
  telefone: string;
}

interface ClientListProps {
  clients: Client[];
}

const ClientList: React.FC<ClientListProps> = ({ clients }) => {
  return (
    <div> 
      <ul>
        {clients.map((client: any, key: any) => (    
          <li key={client.id}>  &nbsp;
            <strong>{key + 1}º</strong>  - {client.nome} - {client.distance.formatted}
          </li>
        ))}
      </ul>
    </div>
  );
};

export default ClientList;
