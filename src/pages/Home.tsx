/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react';
import ClientService from '../services/clientService';
import './Home.css';
import { Table, TableHead, TableBody, TableRow, TableCell, TextField, Button } from '@material-ui/core';
import AddClientModal from './modal/AddClientModal';
import Swal from 'sweetalert2'; 
import EditClientCoordinatesModal from './modal/EditClientCoordinatesModal';
import CalculateRouteModal from './modal/CalculateRouteModal';


interface Client {  
  nome: string;
  email: string;
  telefone: string;
  latitude?: string;
  longitude?: string;
}

const Home: React.FC = () => {
  const [clients, setClients] = useState<Client[]>([]);
  const [searchTerm, setSearchTerm] = useState<string>('');
  const [modalOpen, setModalOpen] = useState<boolean>(false);
  const [modalRouteOpen, setModalRouteOpen] = useState(false);
  const [selectedClientId, setSelectedClientId] = useState<number | null>(null);
  const [editModalOpen, setEditModalOpen] = useState(false);

  useEffect(() => {
    loadClients();
  }, [searchTerm]);

  const loadClients = async () => {
    try {
      const data = await ClientService.getAllClients(searchTerm);
      setClients(data);
    } catch (error: any) {
      console.error('Erro ao carregar clientes:', error.message);
    }
  };

  const handleSearchChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSearchTerm(e.target.value);
  };

  const handleModalOpen = () => {
    setModalOpen(true);
  };

  const handleModalClose = () => {
    setModalOpen(false);
  };

  const handleClientSubmit = async (data: { nome: string; email: string; telefone: string }) => {
    try {
      
      await ClientService.createClient(data);
      
      loadClients();
      Swal.fire({
        icon: 'success',
        title: 'Sucesso!',
        text: 'Cliente cadastrado com sucesso!',
      });
    } catch (error: any) {
      console.error('Erro ao criar cliente:', error.message);
      Swal.fire({
      icon: 'error',
      title: 'Erro!',
      text: 'Ocorreu um erro ao cadastrar o cliente.',
    });
    }
  };

  const handleEditClick = (clientId: number) => {
    setSelectedClientId(clientId);
    setEditModalOpen(true);
  };

  const handleCloseEditModal = () => {
    loadClients();
    setEditModalOpen(false);
  };

  const handleOpenRouteModal = () => {
    setModalRouteOpen(true);
  };

  const handleCloseRouteModal = () => {
    setModalRouteOpen(false);
  };


  return (
    <div className="container">
      <h1>Lista de Clientes</h1>
      <TextField
        type="text"
        value={searchTerm}
        onChange={handleSearchChange}
        placeholder="Pesquisar..."
      />
      <button onClick={handleModalOpen}>Novo Cliente</button>
      <button onClick={handleOpenRouteModal}>Calcular Rota</button>
      
    
      <AddClientModal open={modalOpen} onClose={handleModalClose} onSubmit={handleClientSubmit} />

      <Table className="client-table">
        <TableHead>
          <TableRow> 
            <TableCell className="bold-cell">Nome</TableCell>
            <TableCell className="bold-cell">Email</TableCell>
            <TableCell className="bold-cell">Telefone</TableCell>
            <TableCell className="bold-cell">Coordenada</TableCell>
            <TableCell className="bold-cell">Ações</TableCell> 
          </TableRow>
        </TableHead>
        <TableBody>
          {clients.map((client: any) => (
            <TableRow key={client.id}> 
              <TableCell>{client.nome}</TableCell>
              <TableCell>{client.email}</TableCell>
              <TableCell>{client.telefone}</TableCell>
              <TableCell>{client.latitude},{client.longitude}</TableCell>
              <TableCell>
                <Button onClick={() => handleEditClick(client.id)}>Editar</Button>
              </TableCell> 
            </TableRow>
          ))}
        </TableBody>
      </Table>
      <EditClientCoordinatesModal open={editModalOpen} onClose={handleCloseEditModal} clientId={selectedClientId} />
      <CalculateRouteModal open={modalRouteOpen} onClose={handleCloseRouteModal} />
    </div>
  );
};

export default Home;
