import React, { useState } from 'react';
import { Button, Modal, Box, TextField } from '@mui/material';
import * as yup from 'yup';
import InputMask from 'react-input-mask';

interface AddClientModalProps {
  open: boolean;
  onClose: () => void;
  onSubmit: (data: { nome: string; email: string; telefone: string }) => void;
}
 

const schema = yup.object().shape({
  nome: yup.string().required('O campo "nome" não pode estar vazio'),
  email: yup.string().email('O formato do email é inválido').required('O campo "email" não pode estar vazio'),
  telefone: yup.string().required('O campo "telefone" não pode estar vazio'),
});

const AddClientModal: React.FC<AddClientModalProps> = ({ open, onClose, onSubmit }) => {
  const [nome, setNome] = useState('');
  const [email, setEmail] = useState('');
  const [telefone, setTelefone] = useState('');
  const [errors, setErrors] = useState({
    nome: '',
    email: '',
    telefone: '',
  });

  const handleNomeChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setNome(e.target.value);
    setErrors((prevErrors) => ({ ...prevErrors, nome: '' }));
  };

  const handleEmailChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setEmail(e.target.value);
    setErrors((prevErrors) => ({ ...prevErrors, email: '' }));
  };

  const handleTelefoneChange = (e: React.ChangeEvent<HTMLInputElement>) => { 
    setTelefone(e.target.value); 
  };

  const handleSubmit = async () => {
    try {
      await schema.validate({ nome, email, telefone }, { abortEarly: false });
      setErrors({ nome: '', email: '', telefone: '' });
      onSubmit({ nome, email, telefone });
      onClose();
      setNome('');
      setEmail('');
      setTelefone('');
    } catch (error: any) {
      error.inner.forEach((e: any) => {
        setErrors((prevErrors) => ({ ...prevErrors, [e.path]: e.message }));
      });
    }
  };

  return (
    <Modal open={open} onClose={onClose}>
      <Box
        sx={{
          position: 'absolute',
          top: '50%',
          left: '50%',
          transform: 'translate(-50%, -50%)',
          bgcolor: 'background.paper',
          border: '2px solid #000',
          boxShadow: 24,
          p: 4,
          minWidth: 400,
        }}
      >
        <TextField
          label="Nome"
          value={nome}
          onChange={handleNomeChange}
          fullWidth
          margin="normal"
          error={Boolean(errors.nome)}
          helperText={errors.nome}
        />
        <TextField
          label="Email"
          value={email}
          onChange={handleEmailChange}
          fullWidth
          margin="normal"
          error={Boolean(errors.email)}
          helperText={errors.email}
        />
        <TextField
          label="Telefone"
          value={telefone}
          onChange={handleTelefoneChange}
          fullWidth
          margin="normal"
          error={Boolean(errors.telefone)}
          helperText={errors.telefone}
        />
        <Button variant="contained" onClick={handleSubmit}>
          Salvar
        </Button>
      </Box>
    </Modal>
  );
};

export default AddClientModal;
