import React, { useEffect, useState } from 'react';
import Modal from '@mui/material/Modal';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import ClientList from '../components/ClientList';
import ClientService from '../../services/clientService';

interface CalculateRouteModalProps {
  open: boolean;
  onClose: () => void;
}

interface Client {  
    nome: string;
    email: string;
    telefone: string;
    latitude?: string;
    longitude?: string; 
  }

const CalculateRouteModal: React.FC<CalculateRouteModalProps> = ({ open, onClose }) => {
  const [clientList, setClientList] = useState<Client[]>([]);

  const handleClose = () => {
    onClose();
  };


  useEffect(() => {
    
    const loadClients = async () => {
      try {
        const response: any = await ClientService.getClientsByNearestNeighbor();
        setClientList(response);
      } catch (error: any) {
        console.error('Erro ao calcular a rota:', error.message);
      }
    };

    if (open) {
      loadClients(); 
    }
  }, [open]); 

  return (
    <Modal open={open} onClose={handleClose}>
      <Box sx={{ position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)', bgcolor: 'background.paper', boxShadow: 24, p: 4, minWidth: 400 }}>
        <h2>Calcular Rota </h2>
        <small>Empresa (-23.494936, -47.4697381)</small>
        <ClientList clients={clientList} />
        <Button onClick={handleClose}>Fechar</Button>
      </Box>
    </Modal>
  );
};

export default CalculateRouteModal;
