import React, { useState, useEffect } from 'react';
import { Button, Modal, Box, TextField } from '@mui/material';
import ClientService from '../../services/clientService';
import Swal from 'sweetalert2'; 

interface EditClientCoordinatesModalProps {
  open: boolean;
  onClose: () => void;
  clientId: number | null;
}

const EditClientCoordinatesModal: React.FC<EditClientCoordinatesModalProps> = ({ open, onClose, clientId }) => {
  const [coordinates, setCoordinates] = useState({ latitude: '', longitude: '' });

  useEffect(() => {
    const fetchClientCoordinates = async () => {
      try {
        if (clientId) {
          const client = await ClientService.getClientById(clientId);
          setCoordinates({
            latitude: client.latitude || '',
            longitude: client.longitude || '',
          });
        }
      } catch (error: any) {
        console.error('Erro ao obter coordenadas do cliente:', error.message);
      }
    };

    if (open && clientId) {
      fetchClientCoordinates();
    }
  }, [open, clientId]);

  const handleCoordinateChange = (e: React.ChangeEvent<HTMLInputElement>, coordinate: keyof typeof coordinates) => {
    setCoordinates((prevCoordinates) => ({
      ...prevCoordinates,
      [coordinate]: e.target.value,
    }));
  };

  const handleSubmit = async () => {
    try {
      if (clientId) {
        await ClientService.updateClientCoordinates(clientId, coordinates);
        onClose();
        const Toast = Swal.mixin({
          toast: true,
          position: "top-end",
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          didOpen: (toast: any) => {
            toast.onmouseenter = Swal.stopTimer;
            toast.onmouseleave = Swal.resumeTimer;
          }
        });
        Toast.fire({
          icon: "success",
          title: "Atualizado com sucesso!"
        });
      }
    } catch (error: any) {
      const Toast = Swal.mixin({
        toast: true,
        position: "top-end",
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast: any) => {
          toast.onmouseenter = Swal.stopTimer;
          toast.onmouseleave = Swal.resumeTimer;
        }
      });
      Toast.fire({
        icon: "error",
        title: "Erro ao atualizar coordenadas"
      });
      console.error('Erro ao atualizar coordenadas:', error.message);
    }
  };

  return (
    <Modal open={open} onClose={onClose}>
      <Box
        sx={{
          position: 'absolute',
          top: '50%',
          left: '50%',
          transform: 'translate(-50%, -50%)',
          bgcolor: 'background.paper',
          border: '2px solid #000',
          boxShadow: 24,
          p: 4,
          minWidth: 400,
        }}
      >
        <TextField
          label="Coordenada X"
          value={coordinates.latitude}
          onChange={(e: any) => handleCoordinateChange(e, 'latitude')}
          fullWidth
          margin="normal"
        />
        <TextField
          label="Coordenada Y"
          value={coordinates.longitude}
          onChange={(e: any) => handleCoordinateChange(e, 'longitude')}
          fullWidth
          margin="normal"
        />
        <Button variant="contained" onClick={handleSubmit}>
          Salvar
        </Button>
      </Box>
    </Modal>
  );
};

export default EditClientCoordinatesModal;
