/* eslint-disable import/no-anonymous-default-export */
// clientService.ts

import axios from 'axios'; 


const API_BASE_URL = "http://localhost:3000";

interface Client { 
  nome: string;
  email: string;
  telefone: string;
  latitude?: string;
  longitude?: string;
}

class ClientService {
  async getAllClients(search?: string): Promise<Client[]> {
    const response = await axios.get(`${API_BASE_URL}/clients`, {
      params: { search },
    });
    return response.data;
  }

  async createClient(newClient: Client): Promise<Client> {
    const response = await axios.post(`${API_BASE_URL}/clients`, newClient);
    return response.data;
  }

  async updateClientCoordinates(
    id: number,
    coordinates: { latitude: string; longitude: string }
  ): Promise<void> {
    await axios.put(`${API_BASE_URL}/clients/${id}/coordinates`, coordinates);
  }

  async getClientById (id: number): Promise<Client>{
    try {
      const response = await axios.get(`${API_BASE_URL}/clients/${id}/data`);
      return response.data as Client;
    } catch (error) {
      throw new Error('Erro ao obter detalhes do cliente');
    }
  }

  async getClientsByNearestNeighbor(): Promise<
    { client: Client; distance: number }[]
  > {
    const response = await axios.get(`${API_BASE_URL}/clients/calculate-route`);
    return response.data;
  }
}

export default new ClientService();
